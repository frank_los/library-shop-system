package application.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import config.ApplicationDatabaseConfiguration;

@Import(value={ApplicationWebMvcConfig.class,ApplicationSecurityConfig.class,ApplicationDatabaseConfiguration.class,MailSenderConfiguration.class})
@Configuration
@ComponentScan(basePackages= {"controllers","security","listeners"})
public class ApplicationConfig {

}
