<!DOCTYPE html>
<html>
<head>
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>
	
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
	<script src="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>
</head>
<body>

<div>
  <b-form id="idForm" method="POST" action="/login" @submit="onSubmit" @reset="onReset" v-if="show">
    <b-form-group id="nameFormGroup"
                  label="Username:"
                  label-for="usernameInput">
      <b-form-input id="usernameInput"
                    type="text"
                    v-model="form.username"
                    required
                    name="username"
                    placeholder="Enter your username">
      </b-form-input>
    </b-form-group>
 
    <b-form-group id="passwordFormGroup"
                  label="Password:"
                  label-for="passwordInput">
      <b-form-input id="passwordInput"
                    type="text"
                    v-model="form.password"
                    required
                    name="password"
                    placeholder="Enter your password">
      </b-form-input>
    </b-form-group>
 
    <b-button type="submit" variant="primary">Submit</b-button>
    <b-button type="reset" variant="danger">Reset</b-button>
     
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
  </b-form>
</div>


<script>

var app = new Vue({
  el: '#idForm',
  methods : {
	  	onSubmit: function() {
	        this.$el.submit();
	  	},
	    onReset : function() {
	    	this.form.username = '';
	    	this.form.password = '';
	    }
  },  
  data() {
		return { 
		  	form : {
		  		username: '',
		  		password: ''
		 	},
			show: true
	 	};  
  } 
});

</script>









<%-- 

<html><head><title>Login Page</title></head><body onload='document.f.username.focus();'>
<h3>Login with Username and Password</h3>
<form name='f' action='/login' method='POST'>
<table>
	<tr><td>User:</td><td><input type='text' name='username' value=''></td></tr>
	<tr><td>Password:</td><td><input type='password' name='password'/></td></tr>
	<tr><td colspan='2'><input name="submit" type="submit" value="Login"/></td></tr>
</table>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
<a href="/registration">Click here to register a new account</a> 

 --%>



<br>
<br>
<br>
<br>
<br>
<h1>Next todos</h1>
<ul>
	<li>create a page with a menu, with only one entry : "User management" . there you can handle roles for users</li>
	<li>in the future the pages will be constrained by the role of the logged user</li>
</ul>
<br>
<br>
<br>
<br>
<br>
<h1>Minor todos</h1>
<ul>
	<li>UML of the classes used to configure Spring Security</li>
	<li>save user messages per languages properties file and MessageSource configuration</li>
	<li>handle expiration time for registration requests</li>
</ul>
<br>
<br>
<br>
<br>
<br>
<br>
<h1>Long term todos</h1>
<ul>
	<li>password encoding and review security situations</li>
	<li>add logging using spring aop and method's profiling but only at domain level</li>
	<li>NOT to use domain object into the application service layer, only DTO, presentation models, application event object, etc.
	By this way you can remove the domain-layer from the infrastructure layer's dependencies.</li>
	<li>the hibernate annotations can create problems if set on the fields instead of the getters (version = null and not initialized)</li>
	<li>populate the database massively</li>
</ul>
<br>
<br>
<br>
<br>
<br>
<h1>Considerations</h1>
<ul>
	<li>downside : using ORM affects the domain layer (because of fields like version, delete date, etc) look at BaseDomain</li>
	<li>the ID in entity class MUST be a Long instead of a long. find a way to strict the development to this rule</li>
	<li>SOFT DELETION implementation's considerations: Entity soft deletion has been implemented using the @Where annotation provided by Hibernate 
	although this isn't the best solution. We want to get a pure JPA architecture not coupled with a specific provider. 
	Soft deletion should be a new feature of next Spring Data JPA releases so we could rely on them in the near future.
	</li>
</ul>
</body></html>
