<!DOCTYPE html>
<html>
<head>
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>
	
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
	<script src="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>
</head>
<body>

<b-nav id="app">
  <b-nav-item v-for="item in items" v-bind:to="item.to">{{ item.description }}</b-nav-item>
</b-nav>
	
<script>
	var app = new Vue({
	  el: '#app',
	  data: {
	    items: [
	    	{ description: 'Book', to: '/book-grid' },
	    	{ description: 'Shelf', to: '/hello' },
	    	{ description: 'Account', to: '/hello' }, 
	    ]
	  }
	});
	
</script>
</body>
</html>
