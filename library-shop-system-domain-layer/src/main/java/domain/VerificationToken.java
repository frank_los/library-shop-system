package domain;

import java.util.Calendar;
import java.util.Date;


public class VerificationToken extends BaseDomain {

	private User user;
	private String token;
	private Date expiryDate;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
		
	public static String generateTimestampString() {
		return Calendar.getInstance().getTime().getTime() + "";
	}
}
