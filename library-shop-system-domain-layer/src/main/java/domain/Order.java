package domain;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Order extends BaseDomain {

	private Map<String, Integer> bookCollection = new HashMap<String, Integer>();
	
	private Date orderDate;
	
	
	public void addBook(String isbn, Integer orderQuantity) {
		
		if (orderDate != null) {
			
			throw new RuntimeException("Order already completed, please make another one.");
		}
		
		Boolean isBookInCollection = bookCollection.containsKey(isbn);
		
		if (isBookInCollection) {
			
			Integer actualQuantity = bookCollection.get(isbn);
			bookCollection.put(isbn, actualQuantity + orderQuantity);
		
		} else {
			
			bookCollection.put(isbn, orderQuantity);
			
		}	
		
	}
	
	public void addBook(String isbn) {
		
		addBook(isbn, 1);
		
	}
	
	public void request() {
		
		orderDate = Calendar.getInstance().getTime();
		
	}
}
