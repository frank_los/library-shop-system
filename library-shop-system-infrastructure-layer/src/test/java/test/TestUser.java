package test;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import domain.User;
import services.UserRoleService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes={config.ApplicationDatabaseConfiguration.class})
public class TestUser {

	@Autowired
	private UserRoleService userRoleService;
	
	@Test
	@Transactional
	@Rollback(false)
	public void test() {
		User user = new User();
		user.setEmail("email");
		user.setPassword("password");
		user.setUsername("user");		
		userRoleService.add(user, Locale.ITALIAN, "/test");
	}
}
