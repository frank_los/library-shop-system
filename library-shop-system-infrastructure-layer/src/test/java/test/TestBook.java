package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import controllers.BookPageController;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes={config.ApplicationDatabaseConfiguration.class})
public class TestBook {

	@Autowired
	private BookPageController bookController;
	
	@Test
	@Transactional
	@Rollback(false)
	public void test() {
		
		bookController.doAddBook();
		
		bookController.doGetAllBook();
	}
}
