package repositories;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import com.persistence.base.BaseRepositoryImpl;

import domain.User;
import entities.UserEntity;
import repositories.UserRoleRepository;

@Repository
public class UserRoleRepositoryImpl extends BaseRepositoryImpl<User, UserEntity>  implements UserRoleRepository {
	
	private ModelMapper modelMapper = new ModelMapper();
	
	protected UserRoleRepositoryImpl() {
		super(UserEntity.class);
	}

	@Override
	public User get(String username) {
		return this.get(username, "username");
	}
	
	@Override
	public User get(Long id) {
		return this.get(id, "id");
	}

	@Override
	public User convert(UserEntity userEntity) {
		return modelMapper.map(userEntity, User.class);
	}

	@Override
	public UserEntity convert(User user) {
		return modelMapper.map(user, UserEntity.class);
	}

}
