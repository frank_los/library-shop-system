package repositories;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import com.persistence.base.BaseRepositoryImpl;

import domain.VerificationToken;
import entities.VerificationTokenEntity;

@Repository
public class VerificationTokenRepositoryImpl  extends BaseRepositoryImpl<VerificationToken, VerificationTokenEntity>   implements VerificationTokenRepository {

	private ModelMapper modelMapper = new ModelMapper();
	
	protected VerificationTokenRepositoryImpl() {
		super(VerificationTokenEntity.class);
	}

	@Override
	public VerificationToken convert(VerificationTokenEntity entityObject) {
		return modelMapper.map(entityObject, VerificationToken.class);
	}

	@Override
	public VerificationTokenEntity convert(VerificationToken domainObject) {
		return modelMapper.map(domainObject, VerificationTokenEntity.class);
	}

	@Override
	public VerificationToken get(String token) {
		return get(token, "token");
	}
}
