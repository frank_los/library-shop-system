package repositories;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import com.persistence.base.BaseRepositoryImpl;

import domain.Book;
import entities.BookEntity;
import repositories.BookRepository;

@Repository
public class BookRepositoryImpl extends BaseRepositoryImpl<Book, BookEntity> implements BookRepository {

	private ModelMapper modelMapper = new ModelMapper();
	
	protected BookRepositoryImpl() {
		super(BookEntity.class);
	}

	@Override
	public Book convert(BookEntity entityObject) {
		return modelMapper.map(entityObject, Book.class);
	}

	@Override
	public BookEntity convert(Book domainObject) {
		return modelMapper.map(domainObject, BookEntity.class);
	}

}
