package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.persistence.base.BaseEntity;

@Entity
@Table(name="BOOK")
public class BookEntity extends BaseEntity {

	private Long id;
	private String isbn;
	private String title;
	private String author;
	private String editor;	
	
	@Id
	@SequenceGenerator(name="SQ_BOOK_GENERATOR", sequenceName="SQ_BOOK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_BOOK_GENERATOR")
	@Column(name="ID_BOOK")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="ISBN")
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Column(name="TITLE")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="AUTHOR")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Column(name="EDITOR")
	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}
	
	

}
