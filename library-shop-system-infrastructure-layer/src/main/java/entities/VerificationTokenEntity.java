package entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.persistence.base.BaseEntity;

@Entity
@Table(name = "VERIFICATION_TOKEN")
public class VerificationTokenEntity extends BaseEntity {
	
	private Long id;
	private UserEntity user;
	private String token;
	private Date expiryDate;
		
    @Id
	@SequenceGenerator(name="SQ_VERIFICATION_TOKEN_GENERATOR", sequenceName="SQ_VERIFICATION_TOKEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_VERIFICATION_TOKEN_GENERATOR")
    @Column(name = "ID_VERIFICATION_TOKEN")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    @OneToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(nullable = false, name = "ID_USER")
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

    @Column(name = "TOKEN")
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

    @Column(name = "EXPIRY_DATE")
	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	

}
