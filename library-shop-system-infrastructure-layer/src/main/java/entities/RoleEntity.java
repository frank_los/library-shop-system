package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.persistence.base.BaseEntity;

@Entity
@Table(name = "ROLE")
public class RoleEntity extends BaseEntity {

    private Long id;
    private String role;

    public RoleEntity() {
    }

    @Id
	@SequenceGenerator(name="SQ_ROLE_GENERATOR", sequenceName="SQ_ROLE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ROLE_GENERATOR")
    @Column(name = "ID_ROLE")
    public Long getRoleId() {
        return id;
    }

    public void setRoleId(Long id) {
        this.id = id;
    }

    @Column(name = "ROLE")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
