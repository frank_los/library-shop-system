package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import domain.User;
import services.UserRoleService;

@Controller
public class UserController {
	
	@Autowired
	private UserRoleService userRoleService;
	
	@RequestMapping(value="/hello", method=RequestMethod.POST)
	public String hello() {
		return "hello";
	}

	@RequestMapping(value="/registration", method=RequestMethod.GET)
	public String register() {
		return "registration";
	}
	
	@RequestMapping(value="/add-registration", method=RequestMethod.POST)
	public String addUserRegistration(User user, WebRequest request) {
		userRoleService.add(user, request.getLocale(), request.getContextPath());
		return "registrationSent";
	}
	
	@RequestMapping(value="/confirm-registration", method=RequestMethod.POST)
	public String confirmUserRegistration(@RequestParam String token, WebRequest request) {
		userRoleService.activateUserToken(token);
		return "login"; 
	}
	
}
