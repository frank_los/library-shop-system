package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import domain.Book;
import services.BookService;

@Controller
public class BookPageController {
	
	@Autowired
	BookService bookService;

	@RequestMapping(value="/add", method=RequestMethod.POST)
	public void doAddBook() {		
		Book book = new Book();	
		book.setTitle("A Universe from nothing");
		book.setIsbn("9781471112683");
		book.setEditor("Simon & Schuster UK");
		book.setAuthor("Lawrence M. Krauss");
		bookService.add(book);		
	}
	
	@RequestMapping(value="/get_all", method=RequestMethod.GET)
	public List<Book> doGetAllBook() {
		List<Book> listBook = bookService.getAll();
		return listBook;
	}
	
	@RequestMapping(value="/book-grid", method=RequestMethod.GET)
	public String doGetGridPage() {
		return "book-grid";
	}
	
	

}
