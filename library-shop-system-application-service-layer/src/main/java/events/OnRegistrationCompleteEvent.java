package events;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	private static final long serialVersionUID = 9197137812234379541L;
	
    private String appUrl;
    private Locale locale;
    private Long idUser;
    private String token;

	public OnRegistrationCompleteEvent(Long idUser, String token, Locale locale, String appUrl) {
		super(idUser);
		this.idUser = idUser;
		this.token = token;
		this.locale = locale;
		this.appUrl = appUrl;		
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	
	

}
