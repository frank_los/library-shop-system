package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import domain.Book;
import repositories.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	
	@Transactional(readOnly=true)
	public Book searchByIsbn(String isbn) {		
		return bookRepository.get(isbn, "isbn");		
	}
	
	@Transactional(readOnly=true)
	public List<Book> getAll() {
		return bookRepository.getAll();
	}
	
	@Transactional
	public void add(Book book) {
		bookRepository.add(book);
	}
}
