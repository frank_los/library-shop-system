package services;

import java.util.Map;

import org.springframework.stereotype.Service;

import domain.Order;
import repositories.BookRequestRepository;

@Service
public class OrderService {
	
	private BookRequestRepository<Order> orderRepository;
	
	public Boolean addOrder(Map<String, Integer> map) {

		Order order = new Order();
		
		for (String isbn : map.keySet()) {
			
			Integer quantity = map.get(isbn);
									
			order.addBook(isbn, quantity);
		}
		
		order = orderRepository.add(order);
		
		return order != null;
	}
	
}
