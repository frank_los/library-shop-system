package services;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import domain.User;
import domain.VerificationToken;
import events.OnRegistrationCompleteEvent;
import repositories.UserRoleRepository;
import repositories.VerificationTokenRepository;

@Service
public class UserRoleService {
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired 
	private VerificationTokenRepository verificationTokenRepository;
	
	@Transactional(readOnly=true)
	public User get(String username) {
		return userRoleRepository.get(username);
	}
	
	@Transactional(readOnly=true)
	public User get(Long id) {
		return userRoleRepository.get(id);
	}
	
	@Transactional
	public void add(User user, Locale locale, String appUrl) {	
		
        String token = VerificationToken.generateTimestampString(); // TODO 
        
        VerificationToken verificationToken = new VerificationToken();
		verificationToken.setUser(user);
		verificationToken.setToken(token);	
		verificationToken = verificationTokenRepository.add(verificationToken);	// cascades insert on the user as well
		
		eventPublisher.publishEvent(new OnRegistrationCompleteEvent(verificationToken.getUser().getId(), token, locale, appUrl));
	}
		
	@Transactional
	public void activateUserToken(String token) {
		VerificationToken verificationToken = verificationTokenRepository.get(token);
		if (token.equals(verificationToken.getToken())) {
			verificationToken.getUser().setEnabled(true);
			verificationTokenRepository.set(verificationToken);
		}
	}
		
	
}
