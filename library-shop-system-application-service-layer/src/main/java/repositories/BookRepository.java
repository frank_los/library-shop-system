package repositories;

import java.util.List;

import domain.Book;

public interface BookRepository {
	
	public Book add(Book book);
	
	public void remove(Book book);
	
	public Book get(Object id, String idFieldName);
	
	public List<Book> getAll();

}
