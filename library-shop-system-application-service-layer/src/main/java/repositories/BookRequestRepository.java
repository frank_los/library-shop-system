package repositories;

import java.util.List;

public interface BookRequestRepository<DomainObject>  {

	public DomainObject add(DomainObject book);
	
	public void remove(DomainObject book);
	
	public DomainObject get(Object id, String idFieldName);
	
	public List<DomainObject> getAll();
}
