package repositories;

import domain.VerificationToken;

public interface VerificationTokenRepository {

	public VerificationToken get(String token);
	
	public VerificationToken add(VerificationToken verificationToken);
		
	public VerificationToken set(VerificationToken verificationToken);
	
}
