package repositories;

import domain.User;

public interface UserRoleRepository {

	public User get(String username);
	
	public User get(Long id);
	
	public User add(User user);
		
	public User set(User user);
}
